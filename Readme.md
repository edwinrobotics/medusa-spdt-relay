# Medusa SPDT Relay

![Medusa SPDT Relay](https://shop.edwinrobotics.com/4156-thickbox_default/medusa-spdt-relay.jpg "Medusa SPDT Relay")

[Medusa SPDT Relay](https://shop.edwinrobotics.com/modules/1241-medusa-spdt-relay.html)

The medusa SPDT Single Channel relay module allows you to control high-current AC loads from lower voltage DC control circuitry. We had used 3VDC relay on the module making it much reliable for working with 3.3v Systems. This module will allows you to control your home appliances and other high voltage devices from Mobile App or web using [Medusa Head for ESP32](https://shop.edwinrobotics.com/medusa/1181-medusa-head-for-sparkfun-esp32-thing-esp32-included.html).

FEATURES:

Control voltage: 3V Minimum

Channels: 1

Drive Current:15-20mA

Relay Ratings: AC250V 10A ; DC30V 10A

Indication LED’s for Relay output status



Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.